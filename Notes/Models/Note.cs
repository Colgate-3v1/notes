﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notes.Models
{
	public class Note
	{
		public static Note[] GetNotes()
		{
			return new[]
			{
				new Note(){ Title = "1 попытка", Info = "Не удача!", Date = new DateTime(2023,7,5)},
				new Note(){ Title = "2 попытка", Info = "Не удача!", Date = new DateTime(2023,7,6)},
				new Note(){ Title = "3 попытка", Info = "Удача!", Date = new DateTime(2023,7,7)}
			};
		}
		public string Title { get; set; }
		public string Info { get; set; }
		public DateTime Date { get; set; }
	}
}
