﻿using Notes.Models;
using ReactiveUI;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Reactive;
using System.Threading.Tasks;

namespace Notes.ViewModels
{
	public class MainWindowViewModel : ViewModelBase, INotifyPropertyChanged
	{
		private Note note;
		public MainWindowViewModel()
		{
			MyNotes = new ObservableCollection<Note>(Note.GetNotes());
			DeleteNote = ReactiveCommand.Create<Note>(Delete);
		}
		public ObservableCollection<Note> MyNotes { get; private set; }

		public Note SelectedNote
		{
			get
			{
				return note;
			}
			set
			{
				note = value;
				this.OnPropertyChanged("SelectedNote");
			}
		}
		public event PropertyChangedEventHandler PropertyChanged;
		void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}


		public ReactiveCommand<Note,Unit> DeleteNote { get; }

		public void Delete(Note obj)
		{
			if (SelectedNote != null) { MyNotes.Remove(obj); }
		}
		public Task<string> MyAsyncText => GetTextAsync();
		
		private async Task<string> GetTextAsync()
		{
			await Task.Delay(10000); // The delay is just for demonstration purpose
			return "Time async";
		}
	}
}